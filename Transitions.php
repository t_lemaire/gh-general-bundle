<?php

namespace GorillaHub\GeneralBundle;

/**
 * This class contains static methods for handling transitions or routing, so that you don't have to create an
 * instance of Transition.
 */
class Transitions
{

    /**
     * Split a set of identifiers into two subsets: one identified by $value1 and one identified by $value2.  The
     * percentage of the total set that is identified by $value2 is specified by the $percent parameter.  Also,
     * if this function is called twice with the same identifier, but with two different percentages p1 and p2,
     * and p2 > p1 (that is, the percentage is increased), then if $value1 was returned the first time, it will
     * also be returned the second time.  That way, if $percent increases from 0 to 100, the whole set
     * transitions from $value1 to $value2 with each individual element moving from $value1 to $value2 only once.
     *
     * @param string|int $identifier
     * @param int $percent The percentage of identifiers for which $value2 is returned.
     * @param mixed $value1
     * @param mixed $value2
     * @return mixed Either $value1 or $value2.
     */
    public static function getTransitionalValue($identifier, $percent, $value1, $value2)
    {
        $threshold = $percent / 100;
        return ((65536) * $threshold) > (crc32($identifier) & 65535) ? $value2 : $value1;
    }

}