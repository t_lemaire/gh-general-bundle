<?php
namespace GorillaHub\GeneralBundle;

/**
 * An instance of this class knows how to find a particular item in a tree of nested arrays and objects.  The
 * particular item is called the "target" of the KeyPath.  See KeyPath::$keys for details.
 */
class KeyPath
{
	/**
	 * @var array Each element of this array is a key that should be traversed, from the root, to find the target.
	 * 		For example, if $keys is an empty array, then the target is the root itself.  If $keys is [5, 'abc'],
	 * 		then the target is $keys[5]['abc'].  Note: if possible, use one of the methods of this class instead of
	 * 		accessing $keys directly.
	 */
	public $keys;

	/**
	 * @param KeyPath|array $path1, ... One or more KeyPaths or arrays of keys {@see $keys}.
	 *
	 * @return KeyPath A new path consisting of the dereferences indicated by the first specified path, followed
	 * 		by the dereferences indicated by the second path, etc...  That is, the first path points to a first target,
	 * 		and the second path points to a second target somewhere within the first target, and so on.
	 */
	static public function join($path1) {
		$keys = $path1 instanceof KeyPath ? $path1->keys : $path1;
		for ($i = 1 ; $i < func_num_args() ; $i++) {
			$nextPath = func_get_arg($i);
			$nextKeys = $nextPath instanceof KeyPath ? $nextPath->keys : $nextPath;
			$keys = array_merge($keys, $nextKeys);
		}
		return new KeyPath($keys);
	}

	/**
	 * @param array[] $keys
	 */
	public function __construct($keys) {
		$this->keys = $keys;
	}

	/**
	 * @param mixed $root Any value to traverse to find the target.
	 * @return mixed A reference to the target.  Note that this function can return a reference to a nonexistent
	 *		property of an object or element of an array, as long as that object or array exists.
	 * @throws \Exception if the path cannot be traversed from the root.
	 */
	public function &getReferenceToTarget(&$root) {
		if ($this->keys === []) {
			return $root;
		}
		$ref =& $root;
		$path = '/';
		$keys = $this->keys;
		$lastKey = array_pop($keys);
		foreach ($keys as $key) {
			$path .= $key;
			if (is_array($ref)) {
				if (isset($ref[$key]) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$ref =& $ref[$key];
			} else if (is_object($ref)) {
				if (isset($ref->$key) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$ref =& $ref->$key;
			} else {
				throw new \Exception("The path " . $path . ' does not exist in the given root.');
			}
			$path .= '/';
		}
		if (is_array($ref)) {
			return $ref[$lastKey];
		} else if (is_object($ref)) {
			return $ref->$lastKey;
		}
		throw new \Exception("The path " . $path . ' is not an array or object.');
	}

	/**
	 * @param mixed $root Any value to traverse to find the target.
	 * @return mixed A copy of the target.  If this is changed, the root is not changed, unless what is changed is
	 * 		an object (or something contained by an object).
	 * @throws \Exception if the value does not exist.
	 */
	public function getTarget($root) {
		$value = $root;
		$path = '/';
		foreach ($this->keys as $key) {
			$path .= $key;
			if (is_array($value)) {
				if (isset($value[$key]) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$value = $value[$key];
			} else if (is_object($value)) {
				if (isset($value->$key) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$value = $value->$key;
			} else {
				throw new \Exception("The path " . $path . ' does not exist in the given root.');
			}
			$path .= '/';
		}
		return $value;
	}

	/**
	 * This removes the target element from the root, unless $keys is empty; there is no way for this class to unset
	 * the root.  Note that the target itself does not need to exist, but its container does need to exist.
	 *
	 * @param mixed $root Any value to traverse to find the target.
	 * @throws \Exception if the path cannot be traversed from the root, or if the path refers to the root.
	 */
	public function unsetTarget($root) {
		if ($this->keys === []) {
			throw new \Exception("KeyPath can't unset the root.");
		}
		$ref =& $root;
		$path = '/';
		$keys = $this->keys;
		$lastKey = array_pop($keys);
		$lastRef = null;
		foreach ($keys as $key) {
			$path .= $key;
			if (is_array($ref)) {
				if (isset($ref[$key]) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$ref =& $ref[$key];
			} else if (is_object($ref)) {
				if (isset($ref->$key) === false) {
					throw new \Exception("The path " . $path . ' does not exist in the given root.');
				}
				$ref =& $ref->$key;
			} else {
				throw new \Exception("The path " . $path . ' does not exist in the given root.');
			}
			$path .= '/';
		}
		if (is_array($ref)) {
			unset($ref[$lastKey]);
		} else if (is_object($ref)) {
			unset($ref[$lastKey]);
		} else {
			throw new \Exception("The path " . $path . ' does not exist in the given root.');
		}
	}

	/**
	 * @return bool True iff there are no keys, e.g. the target is the root item.
	 */
	public function pointsToRoot() {
		return $this->keys === [];
	}

	/**
	 * @return KeyPath A copy of this KeyPath with the first key removed.
	 * @throws \Exception
	 */
	public function getWithFirstKeyRemoved() {
		if ($this->keys === []) {
			throw new \Exception("Tried to remove the first key of an empty path.");
		}
		$keys = $this->keys;
		array_shift($keys);
		return new KeyPath($keys);
	}

	/**
	 * @return int|string The first key in the path.
	 * @throws \Exception if the path is empty.
	 */
	public function getFirstKey() {
		if ($this->keys === []) {
			throw new \Exception("Tried to get the first key of an empty path.");
		}
		return reset($this->keys);
	}

	/**
	 * @param $newFirstKey
	 * @return KeyPath A copy of this KeyPath with the specified key prepended.
	 */
	public function getWithKeyPrepended($newFirstKey) {
		$keys = $this->keys;
		array_unshift($keys, $newFirstKey);
		return new KeyPath($keys);
	}

	/**
	 * @param $newLastKey
	 * @return KeyPath A copy of this KeyPath with the specified key appended.
	 */
	public function getWithKeyAppended($newLastKey) {
		$keys = $this->keys;
		$keys[] = $newLastKey;
		return new KeyPath($keys);
	}

	/**
	 * @param KeyPath $other
	 * @return bool True iff this path is the same as the other.
	 */
	public function isSameAs(KeyPath $other) {
		return $this->keys === $other->keys;
	}

	/**
	 * @return string A string that, for debugging purposes, can represent the path.  Note that this is not an
	 * 		injective function; the string has less information than the KeyPath.  For an injective conversion
	 * 		to string, use serialize.
	 */
	public function __toString() {
		$text = '';
		foreach ($this->keys as $key) {
			if (is_int($key)) {
				$text .= '[' . $key . ']';
			} else {
				if ($text !== '') {
					$text .= '.';
				}
				$text .= $key;
			}
		}
		return $text;
	}

	/**
	 * @return int The number of indirections.
	 */
	public function getDepth() {
		return count($this->keys);
	}

}