<?php
namespace GorillaHub\GeneralBundle;

/**
 * This class contains general static methods that are useful for unit testing.
 *
 * @package GorillaHub\GeneralBundle
 */
class PhpUnit
{
	static public function isPhpUnitRunning() {
		$command = Php::stringOrEmpty($_SERVER['argv'][0]);
		if (strpos(pathinfo($command, PATHINFO_BASENAME), 'phpunit') !== false) {
			return true;
		}
		return Php::stringOrEmpty($_SERVER['REQUEST_URI']) === '/phpunit.php';
	}
}