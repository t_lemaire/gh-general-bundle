<?php
namespace GorillaHub\GeneralBundle;

/**
 * An instance of this class, while it exists, throws ErrorException any time a PHP error happens.  Note that
 * nothing else should change the error handler with set_error_handler() while this object exists (unless it also
 * restores the error handler before this object is destructed).
 */
class PhpErrorThrower
{
    /** @var callable */
    private $oldErrorHandler;

    public function __construct() {
        $this->oldErrorHandler = set_error_handler([self::class, 'errorHandler']);
    }

    public function __destruct() {
        set_error_handler($this->oldErrorHandler);
    }

    static public function errorHandler($severity, $message, $file, $line) {
        throw new \ErrorException($message, 0, $severity, $file, $line);
    }
}