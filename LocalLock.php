<?php
namespace GorillaHub\GeneralBundle;

/**
 * @see LocalLocker
 *
 * @package GorillaHub\GeneralBundle
 */
class LocalLock
{
	private $_key;
	private $_locked;

	/**
	 * @param string $key The name of the lock.
	 * @param int|bool $acquireTimeout The number of microseconds before acquiring the lock should fail, or zero if
	 * 		only one non-blocking attempt should be made, or false if it should attempt forever.
	 * @param int|bool|null $keepTimeout The number of microseconds before the lock should automatically be freed, or
	 * 		false if it should exist forever.  If null, the value of $acquireTimeout is used as the keep timeout.
	 * @return LocalLock An object representing the lock.
	 * @throws \Exception if timeout elapses and lock is not locked.
	 */
	public function __construct($key, $acquireTimeout = LocalLocker::DEFAULT_ACQUIRE_TIMEOUT, $keepTimeout = null) {
		$result = LocalLocker::lock($key, $acquireTimeout, $keepTimeout);
		if ($result === false) {
			throw new \Exception($acquireTimeout . " microseconds timeout elapsed while trying to lock " . $key);
		}
		$this->_key = $key;
		$this->_locked = true;
	}

	public function unlock() {
		LocalLocker::unlock($this->_key);
		$this->_locked = false;
	}

	public function __destruct() {
		if ($this->_locked) {
			LocalLocker::unlock($this->_key);
		}
	}
}