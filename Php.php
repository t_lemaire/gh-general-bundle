<?php
namespace GorillaHub\GeneralBundle;

/**
 * This class contains general static methods that relate to PHP as a language, and not to any particular environment
 * or application.  Methods in this class might be candidates for inclusion in the standard PHP library.  However,
 * methods specific to PHP's application as a web language, or as a CLI language, should be placed elsewhere.
 *
 * @package GorillaHub\GeneralBundle
 */
class Php
{
	static public function hex2bin($hex) {
		return pack('H*', $hex);
	}

	static public function stringOrFalse(&$value) {
		return (isset($value) && is_string($value)) ? $value : false;
	}

	static public function stringOrNull(&$value) {
		return (isset($value) && is_string($value)) ? $value : null;
	}

	static public function stringOrEmpty(&$value) {
		return (isset($value) && is_string($value)) ? $value : '';
	}

	static public function stringOrDefault(&$value, $default) {
		return (isset($value) && is_string($value)) ? $value : $default;
	}

	static public function arrayOrFalse(&$value) {
		return (isset($value) && is_array($value)) ? $value : false;
	}

	static public function arrayOrEmpty(&$value) {
		return (isset($value) && is_array($value)) ? $value : array();
	}

	public static function valueOrDefault(&$value, $default) {
		return isset($value) ? $value : $default;
	}

	public static function valueOrNull(&$value) {
		return isset($value) ? $value : null;
	}

	public static function valueOrFalse(&$value) {
		return isset($value) ? $value : false;
	}

	public static function valueOrZero(&$value) {
		return isset($value) ? $value : 0;
	}

	public static function intOrNull(&$value) {
		return Php::isConvertibleToInt($value) ? (int)$value : null;
	}

	public static function floatOrNull(&$value) {
		return isset($value) ? (float)$value : null;
	}

	public static function boolOrNull(&$value) {
		if (!isset($value) || $value === null) {
			return null;
		}
		return $value ? true : false;
	}


	/**
	* This returns true if the specified variable is set and can be converted to an integer with
	* no loss of information.
	*
	* @param mixed $variable
	* @return bool true or false
	*/
	static public function isConvertibleToInt(&$variable) {
		if (!isset($variable) || !is_numeric($variable)) { return false; }
		$v = $variable;
		if (is_string($v) && $v !== '') {
			$v = ltrim($v, '0');
			if ($v === '') { return true; }
		}
		if ((string)(int)$v !== (string)$v) { return false; }
		return true;
	}


	/**
	 * This returns true if the specified variable is set and can be converted to a string.
	 *
	 * @param mixed $variable
	 * @return bool true or false
	 */
	static public function isConvertibleToString(&$variable) {
		if (!isset($variable)) {
			return false;
		}
		return is_string($variable)
		|| is_int($variable)
		|| is_float($variable)
		|| (is_object($variable) && method_exists($variable, '__toString'));
	}

	/**
	* @param mixed $value
	* @return bool True if the value is set, is a string, and contains at least one character.
	*/
	static public function isNonEmptyString(&$value) {
		return (isset($value) && is_string($value) && $value !== '');
	}

	/**
	* This function triggers a user warning that includes information about the specified
	* exception.
	*
	* @param \Exception $e an exception that was thrown.
	*/
	static public function triggerExceptionError($e) {

		$str = $e->getMessage() . "\n" . $e->getTraceAsString();
		trigger_error($str, E_USER_WARNING);
		Php::debugBreak();
	}

	static public function triggerWarningWithBacktrace($message, $level = E_USER_WARNING) {
		trigger_error($message, $level);
		ob_start();
		debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		trigger_error(ob_get_clean(), $level);
		Php::debugBreak();
	}

	static public function debugBreak() {
		if (function_exists('xdebug_break')) {
			xdebug_break();
		}
	}


	/**
	* This function asserts that the given condition is true.  If not, it emits a warning and
	* throws an exception using the specified message.
	*
	* @param bool $condition False if an erroneous condition has been detected.
	* @param string $message A message describing the error condition if $condition is false.
	* @throws \Exception if the assertion failed.
	*/
	static public function assert($condition, $message = "Assert failed") {

		if ($condition === false) {
			Php::triggerWarningWithBacktrace($message, E_USER_WARNING);
			throw new Exceptions\AssertionFailed($message);
		}
	}

	/**
	* @param int $major The major version number (e.g. int(5) from 5.2.7-extra)
	* @param int $minor The minor version number (e.g. int(2) from 5.2.7-extra)
	* @param int $release The release version number (e.g. int(7) from 5.2.7-extra)
	* @return bool True if the current php version is greater than or equal to the specified
	* 		version.
	*/
	static public function isPhpAtLeastVersion($major, $minor, $release) {
		if (PHP_MAJOR_VERSION < $major) { return false; }
		if (PHP_MAJOR_VERSION > $major) { return true; }
		if (PHP_MINOR_VERSION < $minor) { return false; }
		if (PHP_MINOR_VERSION > $minor) { return true; }
		return PHP_RELEASE_VERSION >= $release;
	}

	/**
	 * @param object|array $objectOrArray An object or array that is to be deep-cloned.
	 * @return object|array The cloned object or array.
	 * @throws \InvalidArgumentException if the input object or array is 20 or more levels deep.
	 */
	static public function deepClone($objectOrArray) {
		return self::_deepCloneRecursive($objectOrArray, 0);
	}

	/**
	 * This method appends the specified value to the array, with $array[] = $value, and then returns the new key.
	 *
	 * @param array $array The array to modify.
	 * @param mixed $value The value to append to the array.
	 * @return int The key of the newly inserted value.
	 */
	static public function appendToArray(&$array, $value) {
		$array[] = $value;
		end($array);
		$key = key($array);
		reset($array);
		return $key;
	}

	/**
	 * @param mixed $value
	 * @return string The name of the type of $value, or the name of the class if it is an object.
	 */
	static public function getType(&$value) {
		if (isset($value) && is_object($value)) {
			return get_class($value);
		}
		return gettype($value);
	}

	/**
	 * @param $array1
	 * @param $array2
	 * @return bool True iff every key in $array1 also exists in $array2, and every key in $array2 also exists in
	 * 		$array1.
	 */
	static public function doArraysHaveSameKeys($array1, $array2) {
		if (count($array1) !== count($array2)) {
			return false;
		}
		foreach ($array1 as $key1 => $value1) {
			if (array_key_exists($key1, $array2) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function determines if the specified arrays are identical, disregarding the order of
	 * the keys of the arrays or any sub-arrays.
	 *
	 * @param mixed[] $array1
	 * @param mixed[] $array2
	 * @param string &$message The location to which to write a message that describes the mismatch.
	 * @param string[] $keys Used internally to construct the message.
	 * @return bool
	 */
	static public function areUnorderedArraysSame($array1, $array2, &$message = null, $keys = array()) {
		foreach ($array1 as $key => $value1) {
			if (!array_key_exists($key, $array2)) {
				$keys[] = $key;
				$message = implode('->', $keys) . ' exists in $array1 but not in $array2.';
				return false;
			}
			$value2 = $array2[$key];
			if (is_array($value1) && is_array($value2)) {
				$childKeys = $keys;
				$childKeys[] = $key;
				if (self::areUnorderedArraysSame($value1, $array2[$key], $message, $childKeys) === false) {
					return false;
				}
			} else if ($value1 !== $value2) {
				$keys[] = $key;
				$message = implode('->', $keys) . ' is ' . json_encode($value1) . ' in $array1'
						. ' but ' . json_encode($value2) . ' in $array2.';
				return false;
			}
		}
		foreach ($array2 as $key => $value2) {
			if (!array_key_exists($key, $array1)) {
				$keys[] = $key;
				$message = implode('->', $keys) . ' exists in $array2 but not in $array1.';
				return false;
			}
		}
		return true;
	}



	/**
	 * @see Php::deepClone().
	 */
	static private function _deepCloneRecursive($objectOrArray, $depth) {
		if ($depth === 20) {
			trigger_error("Warning:  Php::deepClone() encountered a depth of 20.", E_USER_WARNING);
			throw new \InvalidArgumentException("Php::deepClone() encountered a depth of 20.");
		}
		if (is_array($objectOrArray)) {
			foreach ($objectOrArray as $key => $value) {
				if (is_object($value) || is_array($value)) {
					$objectOrArray[$key] = Php::_deepCloneRecursive($value, $depth + 1);
				}
			}
		} else {
			$objectOrArray = clone $objectOrArray;
			foreach ($objectOrArray as $key => $value) {
				if (is_object($value) || is_array($value)) {
					$objectOrArray->{$key} = Php::_deepCloneRecursive($value, $depth + 1);
				}
			}
		}
		return $objectOrArray;
	}


	/**
	 * Verifies if needle is in the haystack array. (Case insensitive)
	 * @param string $needle
	 * @param string[] $haystack
	 * @return bool True if needle was found in haystack.
	 */
	public static function inArrayCaseInsensitive($needle, $haystack) {
		$needle = strtolower($needle);

		foreach ($haystack as $value) {
			if ($needle == strtolower($value)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param array $array An array from which to remove all copies of the value.  This array is modified by this
	 * 		function.
	 * @param mixed $value The value to remove.
	 * @param bool $strict True if the comparison is to be done with ===.
	 */
	static public function unsetValueInArray(&$array, $value, $strict) {
		for (;;) {
			$key = array_search($value, $array, $strict);
			if ($key === false) {
				return;
			}
			unset($array[$key]);
		}
	}

	/**
	 * @param array $array An array from which to remove the first copy of the value.  This array is modified by this
	 * 		function.
	 * @param mixed $value The value to remove.
	 * @param bool $strict True if the comparison is to be done with ===.
	 */
	static public function unsetOneValueInArray(&$array, $value, $strict) {
		$key = array_search($value, $array, $strict);
		if ($key !== false) {
			unset($array[$key]);
		}
	}

	/**
	 * @param array $array
	 * @param mixed $value
	 * @param bool $strict True if the comparison is to be done strictly.
	 * @return int The number of elements of $array that are equal to $value.
	 */
	public static function countValueInArray($array, $value, $strict = true)
	{
		$numberOfMatches = 0;
		if ($strict) {
			foreach ($array as $element) {
				if ($element === $value) {
					$numberOfMatches++;
				}
			}
		} else {
			foreach ($array as $element) {
				if ($element == $value) {
					$numberOfMatches++;
				}
			}
		}
		return $numberOfMatches;
	}

	/**
	 * @param array $array1
	 * @param array $array2
	 * @return true True if the arrays are identical after they are both sorted.
	 */
	public static function doArraysHaveSameValues($array1, $array2)
	{
		sort($array1);
		sort($array2);
		return $array1 === $array2;
	}

	/**
	 * @param mixed $value,...
	 * @return string|null The first nonempty string found in any argument, or null if none.
	 */
	public static function getFirstNonEmptyString($value)
	{
		foreach (func_get_args() as $argument) {
			if (Php::isNonEmptyString($argument)) {
				return $argument;
			}
		}
		return null;
	}

	/**
	 * This function calls a given function until it returns or until the number of tries is exhausted.
	 *
	 * @param callable $function The function to be tried and retried.
	 * @param int $numberOfTries The number of tries.  If this is 0, the function will not be called.
	 * @param float $sleep The time to sleep between calls, in seconds.
	 * @return mixed The return value of the successful call, or null if none.
	 * @throws \Exception if all calls to the function throw an exception.  The last exception thrown by
	 * 		the given function is thrown by this function.
	 */
	public static function doWithRetry($function, $numberOfTries, $sleep = 0.0)
	{
		$exception = null;
		for ( ; $numberOfTries > 0 ; $numberOfTries--) {
			try {
				return $function();
			} catch (\Exception $e) {
				$exception = $e;
			}
			if ($sleep > 0 && $numberOfTries > 1) {
				usleep((int)($sleep * 1000000));
			}
		}
		if ($exception !== null) {
			throw $exception;
		}
		return null;
	}

	/**
	 * @param array|object $arrayOrObject
	 * @return array|object A clone of $arrayOrObject but with any properties/elements whose values are null
	 * 		removed.
	 */
	static public function getWithNullValuesUnset($arrayOrObject) {
		$new = is_array($arrayOrObject) ? [] : (object)[];
		foreach ($arrayOrObject as $key => $value) {
			if ($value !== null) {
				if (is_array($arrayOrObject)) {
					$new[$key] = $value;
				} else {
					$new->$key = $value;
				}
			}
		}
		return $new;
	}

    /**
     * This returns $returnIfTrue n% of the time and $returnIfFalse otherwise (100%-n% of the time), where n is given by $percentage
     * example:
     *  1% of the time you want to return true and 99% of the time return false returnByPercentage(1, true, false);
     *
     * @param float|int $percentage must be between 0 and 100
     * @param mixed $returnIfTrue default true
     * @param mixed $returnIfFalse default false
     * @return mixed Either $returnIfTrue or $returnIfFalse.
     */
    public static function returnByPercentage($percentage, $returnIfTrue = true, $returnIfFalse = false)
    {
        if ($percentage < 0 || $percentage > 100) {
            throw new \InvalidArgumentException('Percentage must be between 0 and 100');
        }
        if ($percentage == 100) {
            return $returnIfTrue;
        }
        $floatRand = (mt_rand() / mt_getrandmax()) * 100;

        return ($floatRand < $percentage) ? $returnIfTrue : $returnIfFalse;
    }

	/**
	 * @param array $a
	 * @param array $b
	 * @return array An array containing the values that are in $a, or in $b, but not both.  Note that the
	 * 	keys are NOT preserved.
	 */
	static public function arraySymmetricDiff($a, $b) {
		$d1 = array_diff($a, $b);
		$d2 = array_diff($b, $a);
		return array_values(array_merge($d1, $d2));
	}
}
