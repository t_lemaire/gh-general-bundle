<?php
namespace GorillaHub\GeneralBundle;

/**
 * This class is used to get a mutex that is shared among all threads in the same process.  You should acquire
 * a local lock while a file is locked or while setting a umask in a multithreaded environment.
 *
 * This library requires APC user variables to be enabled to function correctly.  Otherwise, the lock only applies to
 * the current thread.
 *
 * @package GorillaHub\GeneralBundle
 */
class LocalLocker
{
	const DEFAULT_ACQUIRE_TIMEOUT = 1000000;
	const DEFAULT_KEEP_TIMEOUT = 1000000;

	static private $_initialized = false;
	static private $_locks = array();

	/**
	 * @param string $key The name of the lock.
	 * @param int|bool $acquireTimeout The number of microseconds before acquiring the lock should fail, or zero if
	 * 		only one non-blocking attempt should be made, or false if it should attempt forever.
	 * @param int|bool|null $keepTimeout The number of microseconds before the lock should automatically be freed, or
	 * 		false if it should exist forever.  If null, the value of $acquireTimeout is used as the keep timeout.
	 * @return bool true if the lock has been taken, or false if not.
	 */
	static public function lock(
		$key,
		$acquireTimeout = self::DEFAULT_ACQUIRE_TIMEOUT,
		$keepTimeout = null
	) {
		if ($keepTimeout === null) {
			$keepTimeout = $acquireTimeout;
		}
		if ($keepTimeout === 0) {
			self::$_locks[$key] = true;
			return true;
		}
		if ($keepTimeout === false) {
			$keepTimeout = 0;
		}

		self::_init();
		$startTime = microtime();

		if (isset(self::$_locks[$key])) {
			trigger_error("Attempting to lock a key that is already locked.", E_USER_WARNING);
			return false;
		}

		if (self::_isApcSupported() === false) {
			self::$_locks[$key] = true;
			return true;
		}

		for (;;) {
			$result = apc_add('lock:' . $key, 0, (int)ceil($keepTimeout / 1000000));
			if ($result === true) {
				self::$_locks[$key] = true;
				return true;
			}
			$timeElapsed = microtime() - $startTime;
			if ($acquireTimeout !== false && $acquireTimeout >= $timeElapsed) {
				break;
			}
			usleep(1000);
		}

		return false;
	}

	/**
	 * @param string $key The name of the lock.
	 */
	static public function unlock($key) {
		if (isset(self::$_locks[$key]) === false) {
			trigger_error("Attempting to unlock a key that is not locked.", E_USER_WARNING);
			return;
		}
		unset(self::$_locks[$key]);
		if (self::_isApcSupported()) {
			$result = apc_delete('lock:' . $key);
			if ($result === false) {
				trigger_error("Lock " . $key . " was already unlocked in APC.", E_USER_WARNING);
			}
		}
	}

	static public function shutdown() {
		foreach (self::$_locks as $key => $value) {
			self::unlock($key);
		}
	}

	static private function _init() {
		if (self::$_initialized === false) {
			register_shutdown_function(array(__CLASS__, 'shutdown'));
			self::$_initialized = true;
		}
	}

	static private function _isApcSupported() {
		if (function_exists('apc_store') === false) {
			return false;
		}
		apc_store("yes-if-apc-is-supported", "yes");
		return apc_fetch("yes-if-apc-is-supported") === "yes";
	}
}