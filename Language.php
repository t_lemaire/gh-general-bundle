<?php
namespace GorillaHub\GeneralBundle;

class Language
{
	static public function units($quantity, $unit, $pluralSuffix = 's', $singularSuffix = '') {
		return $quantity . ' ' . $unit . (($quantity != 1) ? $pluralSuffix : $singularSuffix);
	}

	/**
	 * @param string[] $strings The strings (i.e. words) to join.
	 * @param bool $oxford True iff the Oxford comma should be used.
	 * @param string $singularSuffix A string to append if $strings has one element.
	 * @param string $pluralSuffix A string to append if $strings has either zero elements or two or more elements.
	 * @return string
	 */
	static public function joinWithAnd(
		$strings,
		$oxford = true,
		$singularSuffix = '',
		$pluralSuffix = ''
	) {
		return self::joinWithConjunction('and', $strings, $oxford, $singularSuffix, $pluralSuffix);
	}

	/**
	 * @param string[] $strings The strings (i.e. words) to join.
	 * @param bool $oxford True iff the Oxford comma should be used.
	 * @param string $singularSuffix A string to append if $strings has one element.
	 * @param string $pluralSuffix A string to append if $strings has either zero elements or two or more elements.
	 * @return string
	 */
	static public function joinWithOr(
		$strings,
		$oxford = true,
		$singularSuffix = '',
		$pluralSuffix = ''
	) {
		return self::joinWithConjunction('or', $strings, $oxford, $singularSuffix, $pluralSuffix);
	}


	/**
	 * @param string $conjunction The conjunction, like "and" or "or".
	 * @param string[] $strings The strings (i.e. words) to join.
	 * @param bool $oxford True iff the Oxford comma should be used.
	 * @param string $singularSuffix A string to append if $strings has one element.
	 * @param string $pluralSuffix A string to append if $strings has either zero elements or two or more elements.
	 * @return string
	 */
	static public function joinWithConjunction(
		$conjunction,
		$strings,
		$oxford = true,
		$singularSuffix = '',
		$pluralSuffix = ''
	) {
		$last = array_pop($strings);
		if ($last === null) {
			return $pluralSuffix;
		}
		if (end($strings) === false) {
			return $last . ($singularSuffix !== '' ? (' ' . $singularSuffix) : '');
		}
		if ($pluralSuffix !== '') {
			$pluralSuffix = ' ' . $pluralSuffix;
		}
		if (prev($strings) === false) {
			return end($strings) . ' ' . $conjunction . ' ' . $last . $pluralSuffix;
		}
		return implode(', ', $strings) . ($oxford ? ', ' : ' ') . $conjunction . ' ' . $last . $pluralSuffix;
	}
}