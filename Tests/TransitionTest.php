<?php
namespace GorillaHub\GeneralBundle\Tests;

use \GorillaHub\GeneralBundle\Transitions;
use PHPUnit\Framework\TestCase;

class TransitionTest extends TestCase
{
    public function testSplitValuesByPercent()
    {
        $this->assertTrue(Transitions::getTransitionalValue('testingisfun', 50, false, true));
        $this->assertFalse(Transitions::getTransitionalValue('asdasddadsfasdfaasdfsfasd', 50, false, true));
    }
}