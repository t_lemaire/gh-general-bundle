<?php
namespace GorillaHub\GeneralBundle\Tests;

use \GorillaHub\GeneralBundle\Strings;
use PHPUnit\Framework\TestCase;

class StringsTest extends TestCase
{
	/**
	 * @dataProvider camelCaseToDashesProvider
	 */
	public function testCamelCaseToDashes($expected, $input) {
		$this->assertSame($expected, Strings::camelCaseToDashes($input));
	}

	public function camelCaseToDashesProvider() {
		return array(
			array('test-camel-case', 'testCamelCase'),
			array('test-camel-case', 'TestCamelCase')
		);
	}

	public function testDoesStringStartWith() {
		$this->assertSame(true, Strings::doesStringStartWith('hats', 'h'));
		$this->assertSame(true, Strings::doesStringStartWith('cats', 'cats'));
		$this->assertSame(false, Strings::doesStringStartWith('gnats', 'gats'));
		$this->assertSame(true, Strings::doesStringStartWith('bats', ''));
	}

	public function testDoesStringEndWith() {
		$this->assertSame(true, Strings::doesStringEndWith('hats', 's'));
		$this->assertSame(true, Strings::doesStringEndWith('cats', 'cats'));
		$this->assertSame(false, Strings::doesStringEndWith('gnats', 'gats'));
		$this->assertSame(true, Strings::doesStringEndWith('bats', ''));
	}

	public function testRemovePrefix() {
		$this->assertSame('ts', Strings::removePrefix('hats', 'ha'));
		$this->assertSame('', Strings::removePrefix('cats', 'cats'));
		try {
			$this->assertSame(false, Strings::removePrefix('gnats', 'gats'));
			$this->assertFalse(true, "Strings::removePrefix should have thrown an exception.");
		} catch (\InvalidArgumentException $e) {
		}
		$this->assertSame(true, Strings::doesStringEndWith('bats', ''));
	}

}