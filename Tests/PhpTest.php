<?php
namespace GorillaHub\GeneralBundle\Tests;

use \GorillaHub\GeneralBundle\Php;
use PHPUnit\Framework\TestCase;

class PhpTest extends TestCase
{
	public function testHex2bin() {
		$this->assertSame("\xF0\x40", Php::hex2bin("F040"));
	}

	public function testStringOrFalse() {
		$a = '';
		$b = 4;
		$this->assertSame('', Php::stringOrFalse($a));
		$this->assertSame(false, Php::stringOrFalse($b));
		$this->assertSame(false, Php::stringOrFalse($c));
	}

	public function testStringOrNull() {
		$a = '';
		$b = 4;
		$this->assertSame('', Php::stringOrNull($a));
		$this->assertSame(null, Php::stringOrNull($b));
		$this->assertSame(null, Php::stringOrNull($c));
	}

	public function testStringOrEmpty() {
		$a = 'hi';
		$b = 4;
		$this->assertSame('hi', Php::stringOrEmpty($a));
		$this->assertSame('', Php::stringOrEmpty($b));
		$this->assertSame('', Php::stringOrEmpty($c));
	}

	public function testStringOrDefault() {
		$a = '';
		$b = 4;
		$this->assertSame('', Php::stringOrDefault($a, 5));
		$this->assertSame(5, Php::stringOrDefault($b, 5));
		$this->assertSame(5, Php::stringOrDefault($c, 5));
	}

	public function testArrayOrFalse() {
		$a = array(4);
		$b = 6;
		$this->assertSame(array(4), Php::arrayOrFalse($a));
		$this->assertSame(false, Php::arrayOrFalse($b));
		$this->assertSame(false, Php::arrayOrFalse($c));
	}

	public function testArrayOrEmpty() {
		$a = array(4);
		$b = 6;
		$this->assertSame(array(4), Php::arrayOrEmpty($a));
		$this->assertSame(array(), Php::arrayOrEmpty($b));
		$this->assertSame(array(), Php::arrayOrEmpty($c));
	}

	public function testValueOrDefault() {
		$a = 5;
		$this->assertSame(5, Php::valueOrDefault($a, 6));
		$this->assertSame(6, Php::valueOrDefault($b, 6));
	}

	public function testValueOrNull() {
		$a = 5;
		$this->assertSame(5, Php::valueOrNull($a));
		$this->assertSame(null, Php::valueOrNull($b));
	}

	public function testValueOrFalse() {
		$a = 5;
		$this->assertSame(5, Php::valueOrFalse($a));
		$this->assertSame(false, Php::valueOrFalse($b));
	}

	public function testValueOrZero() {
		$a = 5;
		$this->assertSame(5, Php::valueOrZero($a));
		$this->assertSame(0, Php::valueOrZero($b));
	}

	public function testIntOrNull() {
		$a = 4;
		$b = '6';
		$c = 'cat';
		$this->assertSame(4, Php::intOrNull($a));
		$this->assertSame(6, Php::intOrNull($b));
		$this->assertSame(null, Php::intOrNull($c));
		$this->assertSame(null, Php::intOrNull($d));
	}

	public function testBoolOrNull() {
		$a = 4;
		$b = 0;
		$this->assertSame(true, Php::boolOrNull($a));
		$this->assertSame(false, Php::boolOrNull($b));
		$this->assertSame(null, Php::boolOrNull($c));
	}

	/**
	 * @dataProvider isConvertibleToIntProvider
	 */
	public function testIsConvertibleToInt($in, $out) {
		$inCopy = $in;
		$this->assertSame($out, Php::isConvertibleToInt($in));
		$this->assertTrue($inCopy === $in);
	}

	public function isConvertibleToIntProvider() {
		return array(
				array(4, true),
				array("5", true),
				array("02", true),
				array(" 04", false),
				array("04 ", false),
				array("0", true),
				array("00", true),
				array("", false)
				);
	}

	/**
	 * @dataProvider isNonEmptyStringProvider
	 */
	public function testIsNonEmptyString($expected, $input) {
		$this->assertSame($expected, Php::isNonEmptyString($input));
		$this->assertSame(false, Php::isNonEmptyString($nonexistent));
	}

	public function isNonEmptyStringProvider() {
		return array(
			array(true, 'cat'),
			array(false, ''),
			array(false, 4),
			array(false, array())
		);
	}

	public function testDeepClone()
	{
		$object = new \StdClass();
		$object->a = array('a');
		$object->b = new \StdClass();
		$object->b->d = 'd';

		$clone = Php::deepClone($object);
		$clone->c = 'c';
		$clone->a[] = 'b';
		$clone->b->e = 'e';

		$this->assertFalse(isset($object->c));
		$this->assertEquals(array('a'), $object->a);
		$this->assertFalse(isset($object->b->e));
	}

	public function testAppendToArray() {
		$array = array(4 => 'four');
		$this->assertSame(5, Php::appendToArray($array, 'five'));
		$this->assertSame(array(4 => 'four', 5 => 'five'), $array);
	}

	public function testinArrayCaseInsensitive() {
		$array = array('apple', 'Banana', 'DURIAN', 'OrAnGe', 'STRAWberry');

		$this->assertTrue(Php::inArrayCaseInsensitive('Apple', $array));
		$this->assertTrue(Php::inArrayCaseInsensitive('durian', $array));
		$this->assertTrue(Php::inArrayCaseInsensitive('STRAWBERRY', $array));
		$this->assertFalse(Php::inArrayCaseInsensitive('Tomato', $array));
	}
}