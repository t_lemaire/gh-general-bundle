<?php
namespace GorillaHub\GeneralBundle\Time;

/**
 * An instance of this class can be used to call various built-in time functions in a predictable timezone without
 * affecting the global timezone setting (i.e. so that you don't break the rest of the project).
 */
class TimeFunctions
{
	private $timezone;

	public function __construct($timezone) {
		$this->timezone = $timezone;
	}

	/**
	 * Get Unix timestamp for a date
	 * @link http://php.net/manual/en/function.mktime.php
	 * @param int $hour [optional] <p>
	 * The number of the hour.
	 * </p>
	 * @param int $minute [optional] <p>
	 * The number of the minute.
	 * </p>
	 * @param int $second [optional] <p>
	 * The number of seconds past the minute.
	 * </p>
	 * @param int $month [optional] <p>
	 * The number of the month.
	 * </p>
	 * @param int $day [optional] <p>
	 * The number of the day.
	 * </p>
	 * @param int $year [optional] <p>
	 * The number of the year, may be a two or four digit value,
	 * with values between 0-69 mapping to 2000-2069 and 70-100 to
	 * 1970-2000. On systems where time_t is a 32bit signed integer, as
	 * most common today, the valid range for year
	 * is somewhere between 1901 and 2038. However, before PHP 5.1.0 this
	 * range was limited from 1970 to 2038 on some systems (e.g. Windows).
	 * </p>
	 * @param int $is_dst [optional] <p>
	 * Deprecated since 5.3.0 - use Use the new timezone handling functions instead.<p>
	 * This parameter can be set to 1 if the time is during daylight savings time (DST),
	 * 0 if it is not, or -1 (the default) if it is unknown whether the time is within
	 * daylight savings time or not. If it's unknown, PHP tries to figure it out itself.
	 * This can cause unexpected (but not incorrect) results.
	 * Some times are invalid if DST is enabled on the system PHP is running on or
	 * is_dst is set to 1. If DST is enabled in e.g. 2:00, all times
	 * between 2:00 and 3:00 are invalid and mktime returns an undefined
	 * (usually negative) value.
	 * Some systems (e.g. Solaris 8) enable DST at midnight so time 0:30 of the day when DST
	 * is enabled is evaluated as 23:30 of the previous day.
	 * </p>
	 * <p>
	 * As of PHP 5.1.0, this parameter became deprecated. As a result, the
	 * new timezone handling features should be used instead.
	 * </p>
	 * @return int mktime returns the Unix timestamp of the arguments
	 * given.
	 * If the arguments are invalid, the function returns false (before PHP 5.1
	 * it returned -1).
	 * @since 4.0
	 * @since 5.0
	 */
	public function mktime(
		$hour = null,
		$minute = null,
		$second = null,
		$month = null,
		$day = null,
		$year = null,
		$is_dst = null
	) {
		return $this->callSystemFunction('mktime', func_get_args());
	}

	/**
	 * Format a local time/date
	 * @link http://php.net/manual/en/function.date.php
	 * @param string $format <p>
	 * The format of the outputted date string. See the formatting
	 * options below. There are also several
	 * predefined date constants
	 * that may be used instead, so for example DATE_RSS
	 * contains the format string 'D, d M Y H:i:s'.
	 * </p>
	 * <p>
	 * The following characters are recognized in the
	 * format parameter string
	 * <table>
	 * <tr valign="top">
	 * <td>format character</td>
	 * <td>Description</td>
	 * <td>Example returned values</td>
	 * </tr>
	 * <tr valign="top">
	 * Day</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>d</td>
	 * <td>Day of the month, 2 digits with leading zeros</td>
	 * <td>01 to 31</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>D</td>
	 * <td>A textual representation of a day, three letters</td>
	 * <td>Mon through Sun</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>j</td>
	 * <td>Day of the month without leading zeros</td>
	 * <td>1 to 31</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>l (lowercase 'L')</td>
	 * <td>A full textual representation of the day of the week</td>
	 * <td>Sunday through Saturday</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>N</td>
	 * <td>ISO-8601 numeric representation of the day of the week (added in
	 * PHP 5.1.0)</td>
	 * <td>1 (for Monday) through 7 (for Sunday)</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>S</td>
	 * <td>English ordinal suffix for the day of the month, 2 characters</td>
	 * <td>
	 * st, nd, rd or
	 * th. Works well with j
	 * </td>
	 * </tr>
	 * <tr valign="top">
	 * <td>w</td>
	 * <td>Numeric representation of the day of the week</td>
	 * <td>0 (for Sunday) through 6 (for Saturday)</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>z</td>
	 * <td>The day of the year (starting from 0)</td>
	 * <td>0 through 365</td>
	 * </tr>
	 * <tr valign="top">
	 * Week</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>W</td>
	 * <td>ISO-8601 week number of year, weeks starting on Monday (added in PHP 4.1.0)</td>
	 * <td>Example: 42 (the 42nd week in the year)</td>
	 * </tr>
	 * <tr valign="top">
	 * Month</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>F</td>
	 * <td>A full textual representation of a month, such as January or March</td>
	 * <td>January through December</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>m</td>
	 * <td>Numeric representation of a month, with leading zeros</td>
	 * <td>01 through 12</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>M</td>
	 * <td>A short textual representation of a month, three letters</td>
	 * <td>Jan through Dec</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>n</td>
	 * <td>Numeric representation of a month, without leading zeros</td>
	 * <td>1 through 12</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>t</td>
	 * <td>Number of days in the given month</td>
	 * <td>28 through 31</td>
	 * </tr>
	 * <tr valign="top">
	 * Year</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>L</td>
	 * <td>Whether it's a leap year</td>
	 * <td>1 if it is a leap year, 0 otherwise.</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>o</td>
	 * <td>ISO-8601 year number. This has the same value as
	 * Y, except that if the ISO week number
	 * (W) belongs to the previous or next year, that year
	 * is used instead. (added in PHP 5.1.0)</td>
	 * <td>Examples: 1999 or 2003</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>Y</td>
	 * <td>A full numeric representation of a year, 4 digits</td>
	 * <td>Examples: 1999 or 2003</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>y</td>
	 * <td>A two digit representation of a year</td>
	 * <td>Examples: 99 or 03</td>
	 * </tr>
	 * <tr valign="top">
	 * Time</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>a</td>
	 * <td>Lowercase Ante meridiem and Post meridiem</td>
	 * <td>am or pm</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>A</td>
	 * <td>Uppercase Ante meridiem and Post meridiem</td>
	 * <td>AM or PM</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>B</td>
	 * <td>Swatch Internet time</td>
	 * <td>000 through 999</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>g</td>
	 * <td>12-hour format of an hour without leading zeros</td>
	 * <td>1 through 12</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>G</td>
	 * <td>24-hour format of an hour without leading zeros</td>
	 * <td>0 through 23</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>h</td>
	 * <td>12-hour format of an hour with leading zeros</td>
	 * <td>01 through 12</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>H</td>
	 * <td>24-hour format of an hour with leading zeros</td>
	 * <td>00 through 23</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>i</td>
	 * <td>Minutes with leading zeros</td>
	 * <td>00 to 59</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>s</td>
	 * <td>Seconds, with leading zeros</td>
	 * <td>00 through 59</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>u</td>
	 * <td>Microseconds (added in PHP 5.2.2)</td>
	 * <td>Example: 654321</td>
	 * </tr>
	 * <tr valign="top">
	 * Timezone</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>e</td>
	 * <td>Timezone identifier (added in PHP 5.1.0)</td>
	 * <td>Examples: UTC, GMT, Atlantic/Azores</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>I (capital i)</td>
	 * <td>Whether or not the date is in daylight saving time</td>
	 * <td>1 if Daylight Saving Time, 0 otherwise.</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>O</td>
	 * <td>Difference to Greenwich time (GMT) in hours</td>
	 * <td>Example: +0200</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>P</td>
	 * <td>Difference to Greenwich time (GMT) with colon between hours and minutes (added in PHP 5.1.3)</td>
	 * <td>Example: +02:00</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>T</td>
	 * <td>Timezone abbreviation</td>
	 * <td>Examples: EST, MDT ...</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>Z</td>
	 * <td>Timezone offset in seconds. The offset for timezones west of UTC is always
	 * negative, and for those east of UTC is always positive.</td>
	 * <td>-43200 through 50400</td>
	 * </tr>
	 * <tr valign="top">
	 * Full Date/Time</td>
	 * <td>---</td>
	 * <td>---</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>c</td>
	 * <td>ISO 8601 date (added in PHP 5)</td>
	 * <td>2004-02-12T15:19:21+00:00</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>r</td>
	 * <td>RFC 2822 formatted date</td>
	 * <td>Example: Thu, 21 Dec 2000 16:01:07 +0200</td>
	 * </tr>
	 * <tr valign="top">
	 * <td>U</td>
	 * <td>Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)</td>
	 * <td>See also time</td>
	 * </tr>
	 * </table>
	 * </p>
	 * <p>
	 * Unrecognized characters in the format string will be printed
	 * as-is. The Z format will always return
	 * 0 when using gmdate.
	 * </p>
	 * <p>
	 * Since this function only accepts integer timestamps the
	 * u format character is only useful when using the
	 * date_format function with user based timestamps
	 * created with date_create.
	 * </p>
	 * @param int $timestamp [optional] The optional timestamp parameter is an integer Unix timestamp
	 * that defaults to the current local time if a timestamp is not given.
	 * In other words, it defaults to the value of time().
	 * @return string|bool a formatted date string. If a non-numeric value is used for
	 * timestamp, false is returned and an
	 * E_WARNING level error is emitted.
	 * @since 4.0
	 * @since 5.0
	 */
	function date($format, $timestamp = null) {
		return $this->callSystemFunction('date', func_get_args());
	}

	/**
	 * @param int $unixTimestamp
	 * @return bool|string A string in the form "March 8, 2014, 4:22 PM"
	 */
	public function getDateTimeString($unixTimestamp) {
		$tz = new TimeZone($this->timezone);
		return date('F j, Y, g:i A', $unixTimestamp);
	}

	/**
	 * Parse about any English textual datetime description into a Unix timestamp
	 * @link http://php.net/manual/en/function.strtotime.php
	 * @param string $time <p>
	 * The string to parse. Before PHP 5.0.0, microseconds weren't allowed in
	 * the time, since PHP 5.0.0 they are allowed but ignored.
	 * </p>
	 * @param int $now [optional] <p>
	 * The timestamp which is used as a base for the calculation of relative
	 * dates.
	 * </p>
	 * @return int a timestamp on success, false otherwise. Previous to PHP 5.1.0,
	 * this function would return -1 on failure.
	 * @since 4.0
	 * @since 5.0
	 */
	function strtotime($time, $now = null) {
		return $this->callSystemFunction('strtotime', func_get_args());
	}

	/**
	 * @param string $functionName The name of the system function to call.
	 * @param mixed[] $args The arguments, with optional arguments specified with nulls.
	 * @return mixed The return value of the system function.
	 */
	private function callSystemFunction($functionName, $args) {
		$tz = new TimeZone($this->timezone);
		while (end($args) === null) {
			array_pop($args);
		}
		return call_user_func_array($functionName, $args);
	}

	/**
	 * @param int $time A unix timestamp.
	 * @return int The unix timestamp of the beginning of the day on which $time occurs.
	 */
	public function getBeginningOfDay($time) {
		$year = (int)$this->date('Y', $time);
		$month = (int)$this->date('m', $time);
		$day = (int)$this->date('d', $time);
		return $this->mktime(0, 0, 0, $month, $day, $year);
	}

	/**
	 * @param int $time A unix timestamp.
	 * @return int The unix timestamp of the beginning of the week (midnight on Sunday) on which $time occurs.
	 */
	public function getBeginningOfWeek($time) {
		$time = $this->getBeginningOfDay($time);
		$weekday = (int)date('w', $time);
		return $time - (86400 * $weekday);
	}

	/**
	 * @param int $time A unix timestamp.
	 * @return int The unix timestamp of the beginning of the month on which $time occurs.
	 */
	public function getBeginningOfMonth($time) {
		$year = (int)$this->date('Y', $time);
		$month = (int)$this->date('m', $time);
		return $this->mktime(0, 0, 0, $month, 1, $year);
	}
}