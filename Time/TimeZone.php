<?php
namespace GorillaHub\GeneralBundle\Time;

/**
 * An instance of this class temporarily overrides the PHP default timezone while the instance of this class exists.
 * When the object destructs, the timezone is restored to whatever it was.
 */
class TimeZone
{
	private $originalTimeZone;

	public function __construct($timezone) {
		$this->originalTimeZone = @date_default_timezone_get();
		date_default_timezone_set($timezone);
	}

	public function __destruct() {
		date_default_timezone_set($this->originalTimeZone);
	}
}