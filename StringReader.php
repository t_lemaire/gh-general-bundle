<?php
namespace GorillaHub\GeneralBundle;

/**
 * An instance of this class is used to read incrementally from a string.  It remembers the string and an offset into
 * the string.  Operations are provided for operating on the string at its current position and for advancing
 * the offset appropriately.
 */
class StringReader
{
	/** @var string */
	public $string;

	/** @var int */
	public $offset;

	/** @var int The length of the string. */
	public $length;

	public function __construct($string, $offset = 0) {
		$this->string = $string;
		$this->offset = $offset;
		$this->length = strlen($string);
	}

	/**
	 * @return bool True if the reader has reached the end of the string.
	 */
	public function isAtEnd() {
		return $this->offset >= $this->length;
	}

	/**
	 * @return bool True if the reader has not reached the end of the string.
	 */
	public function isNotAtEnd() {
		return $this->offset < $this->length;
	}

	/**
	 * @return string The next byte.
	 */
	public function peekChar() {
		return $this->string[$this->offset];
	}

	/**
	 * This reads the next byte and advances the current offset.
	 *
	 * @return string The next byte.
	 */
	public function readChar() {
		return $this->string[$this->offset++];
	}

	/**
	 * @param int $length
	 * @return string The next $length bytes, or all remaining bytes, whichever is fewer.
	 */
	public function peek($length) {
		return substr($this->string, $this->offset, $length);
	}

	/**
	 * This reads the next $length bytes and advances the current offset.
	 *
	 * @param $length
	 * @return string The next $length bytes, or all remaining bytes, whichever is fewer.
	 */
	public function read($length) {
		$string = substr($this->string, $this->offset, $length);
		$this->offset += strlen($string);
		return $string;
	}

	/**
	 * @param int $offset The offset relative to the beginning of the string.
	 * @return string The following bytes until (but not including) the byte at $offset.
	 */
	public function peekTo($offset) {
		return substr($this->string, $this->offset, $offset - $this->offset);
	}


	/**
	 * This returns the rest of the string, without moving the offset to the end.
	 *
	 * @return string The rest of the string.
	 */
	public function peekToEnd() {
		return substr($this->string, $this->offset);
	}

	/**
	 * This reads until the specified offset and advances the current offset.
	 *
	 * @param int $offset The offset relative to the beginning of the string.
	 * @return string The following bytes until (but not including) the byte at $offset.
	 */
	public function readTo($offset) {
		$string = substr($this->string, $this->offset, $offset - $this->offset);
		$this->offset = $offset;
		return $string;
	}

	/**
	 * This returns the rest of the string, moving the offset to the end.
	 *
	 * @return string The rest of the string.
	 */
	public function readToEnd() {
		$remaining = substr($this->string, $this->offset);
		$this->offset = $this->length;
		return $remaining;
	}

	/**
	 * This moves the current position to the end of the string.
	 */
	public function seekToEnd() {
		$this->offset = $this->length;
	}

	/**
	 * This behaves like preg_match() except that it searches from the current read position.  If you want to find
	 * a string that begins exactly at the current read position, the regex must begin with \G.
	 *
	 * @param string $pattern
	 * @param mixed $matches
	 * @param int $flags
	 * @param int $additionalOffset The offset from the current reading position from which to read.
	 * @return int The number of matches (0 or 1)
	 */
	public function pregMatch($pattern, &$matches, $flags, $additionalOffset = 0) {
		return preg_match($pattern, $this->string, $matches, $flags, $this->offset + $additionalOffset);
	}

}