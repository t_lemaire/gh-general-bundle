<?php

namespace GorillaHub\GeneralBundle\Gdprhub;


class RequestAddons
{

    /**
     * @var array
     */
    private $server;

    /**
     * RequestAddons constructor.
     * @param array $server
     */
    public function __construct(array $server)
    {
        $this->server = $server;
    }


    /**
     * @return string - real user IP
     */
    public function getRealIp()
    {
        return $this->getIp(true);
    }

    /**
     * @return string - obfuscated user IP
     */
    public function getMaskedIp()
    {
        return $this->getIp(false);
    }

    /**
     * @param bool $real
     * @return string
     */
    private function getIp($real = false)
    {
        $key = $real ? ServerVariables::REAL_IP : ServerVariables::MASKED_IP;
        return array_key_exists($key, $this->server) ? $this->server[$key] : '';
    }

}