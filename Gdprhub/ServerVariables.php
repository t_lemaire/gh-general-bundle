<?php

namespace GorillaHub\GeneralBundle\Gdprhub;


class ServerVariables
{

    /**
     * Server value name for masked IP
     * Note that REMOTE_ADDR will have the same value
     * Value will be obfuscated for EU users
     */
    const MASKED_IP = 'HTTP_X_RN_XFF';


    /**
     * Server value for real user IP
     * Value will always represent real IP
     */
    const REAL_IP = 'HTTP_X_RN_FRAUDIP';

}