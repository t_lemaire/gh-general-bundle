<?php

namespace GorillaHub\GeneralBundle;

/**
 * An instance of this class is used to gradually transition a value that is associated with a set of items, so that
 * the value is not changed for all items at the same time.  For example, the "value" can be a hostname or query
 * parameter used to generate a link to an asset on a CDN, and the "items" would be the assets.  You can use this
 * class to gradually change the links to your assets so that you don't overload the origins by switching everything
 * at once.
 *
 * To use this class, create an instance of the class and specify the initial value (for example, the hostname that
 * you are already using in your links).  Then call addTransitionPeriod() to specify the period of time during
 * which the transition should take place, and the new value (for example, the new hostname) that you want to use
 * after the transition is complete.  The start time must be set to some time after you deploy the code that uses
 * this class; if not, then some fraction of your items will transition to the new value all at once when you deploy.
 * If you use strtotime() to generate your timestamps, you are advised to specify the timezone explicitly.
 *
 * Once the transition is set up, call getCurrentValue() one or more times to get the values that should be used to
 * generate your links.  The function assigns a different transition time (some time between the transition start and
 * end time) to every item, based on the itemIdentifier.  The new value is returned after the specified item's own
 * transition time.  The time is assigned deterministically based on the item identifier, so it is important to always
 * pass the same identifier for each item.  For example, if you use the relative path of your item as the identifier,
 * like 'myimages/502.jpg' (without a leading slash), then you cannot begin to use '/myimages/502.jpg' (with a leading
 * slash) in middle of the transition.
 *
 * If you make a mistake, and the transition has already started, the situation has to be handled carefully.  Any
 * change you make to the code, including reverting the transition, will cause a sudden change in your links.  One
 * safe way to deal with mistakes is to add another call to addTransitionPeriod() _after_ your existing calls to that
 * method, and pass a start time is _after_ the time at which the new call is deployed.  If two transitions X and Y
 * are added with addTransitionPeriod(), in that order, then this class considers the transition X to "freeze" at the
 * start time of Y, and a new transition begins from the value which is the (frozen) output of transition X to the new
 * value of transition Y.  After the end time specified in the last call to addTransitionPeriod(), getCurrentValue()
 * will always return the new value specified in that last call to addTransitionPeriod(), even if earlier calls to
 * addTransitionPeriod() have end times that are later.
 *
 * In short, follow these rules to handle mistakes:
 * 	- Do not add any calls to addTransitionPeriod() except after all other calls to it (if any), and make sure the
 * 		start time of any new call is after the time at which the new call is deployed.
 *  - Do not alter or remove any calls to addTransitionPeriod() unless the start time passed to the call is after your
 * 		deployment time.
 */
class Transition
{
	/** @var mixed The initial value. */
	private $startValue;

	/**
	 * @var int[][] An array of transitions, in reverse order.  Each transition is an array with the following
	 * 		properties:
	 * 			'startTime' => The time at which the transition starts.
	 * 			'endTime' => The time at which the transition ends.
	 * 			'newValue' => The value to which to switch.
	 */
	private $transitionPeriods = [];

	/** @var int|null The current autodetected time, or null if not autodetected yet. */
	private $now = null;

	/**
	 * @param mixed $startValue Any value to return from getCurrentValue() before the first transition time.
	 */
	public function __construct($startValue) {
		$this->startValue = $startValue;
	}

	/**
	 * @param int $startTime The unix timestamp at which to begin transitioning to the new value.
	 * @param int $endTime The unix timestamp after which the new value should always be returned.
	 * @param mixed $newValue The value that should be returned from getCurrentValue() after the transition.
	 * @return $this;
	 */
	public function addTransitionPeriod($startTime, $endTime, $newValue) {
		array_unshift(
			$this->transitionPeriods,
			[
				'startTime' => $startTime,
				'endTime' => $endTime,
				'newValue' => $newValue
			]
		);
		return $this;
	}

	/**
	 * @param string $itemIdentifier A string that identifies the item whose corresponding value should be returned.
	 * @param null $now The unix timestamp of the current time (e.g. for testing), or null to detect automatically.
	 * @return mixed The current value for the specified item.
	 * @throws \LogicException if addTransitionPeriod() was never called.
	 */
	public function getCurrentValue($itemIdentifier, $now = null) {
		if ($this->transitionPeriods === []) {
			throw new \LogicException("Tried calling Transition::getCurrentValue() without any transition periods.");
		}
		if ($now === null) {
			if ($this->now === null) {
				$this->now = time();
			}
			$now = $this->now;
		}
		$hash = crc32($itemIdentifier);
		$transitionPoint = $hash / 0x100000000;
		foreach ($this->transitionPeriods as $transition) {
			if ($now >= $transition['endTime']) {
				return $transition['newValue'];
			}
			if ($now >= $transition['startTime']) {
				$currentPoint = ($now - $transition['startTime']) / ($transition['endTime'] - $transition['startTime']);
				if ($currentPoint >= $transitionPoint) {
					return $transition['newValue'];
				}
			}
			$now = min($now, $transition['startTime']);
		}
		return $this->startValue;
	}

}