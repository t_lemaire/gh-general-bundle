<?php
namespace GorillaHub\GeneralBundle;

/**
 * @package GorillaHub\GeneralBundle
 *
 * This is a base class for an enumeration type. The implementing type:
 *
 * 1) must define a protected property called $_names of type string[].  This is an array where, for each element, the
 * 		key is one of the possible values of the enumeration, and the value is a string that describes the meaning of
 * 		that value; and
 * 2) should define one public constant for each of the possible values of the enumeration.
 *
 * Example:
 *
 *	class SoupType extends AbstractEnum
 *	{
 *		const GAZPACHO = 0
 *		const MINESTRONE = 1
 *		const RAMEN = 2
 *
 *		protected $_names = array(
 *			self::GAZPACHO => "Gazpacho",
 *			self::MINESTRONE => "Minestrone",
 *			self::RAMEN => "Ramen"
 *		);
 * 	}
 */
abstract class AbstractEnum {

	private $_enumValue;

	public function __construct($value) {
		if ($value instanceof $this) {
			$this->_enumValue = $value->getValue();
		} else {
			if (static::isValueValid($value) === false) {
				throw new \Exception('Invalid ' . get_class($this) . ' value ' . $value);
			}
			$this->_enumValue = $value;
		}
	}

	public function getValue() {
		return $this->_enumValue;
	}

	static public function isValueValid($enumValue) {
		return isset(static::$_names[$enumValue]);
	}

	static public function getName($enumValue) {
		return Php::valueOrFalse(static::$_names[$enumValue]);
	}

	static public function getNames() {
		return static::$_names;
	}

	static public function getCount() {
		return count(static::$_names);
	}

	static public function getValues() {
		return array_keys(static::$_names);
	}

}