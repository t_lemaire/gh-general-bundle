<?php

namespace GorillaHub\GeneralBundle;

/**
 * This class contains methods related to strings.
 *
 * @package GorillaHub\GeneralBundle
 */
class Strings
{
	/**
	 * This function changes a lowerCamelCase or UpperCamelCase string to a dash-separated string.  For example,
	 * "thisIsCamelCase" and "ThisIsCamelCase" both become "this-is-camel-case".
	 *
	 * @param string $string The string to modify.
	 * @return string The modified string.
	 */
	static public function camelCaseToDashes($string) {
		$l = strlen($string);
		$newString = strtolower((string)substr($string, 0, 1));
		for($i = 1 ; $i < $l ; $i++) {
			$c = $string[$i];
			if (ctype_upper($c)) {
				$newString .= '-' . strtolower($c);
			} else {
				$newString .= $c;
			}
		}
		return $newString;
	}


	/**
	 * @param string $string The string to examine.
	 * @param string $subString The suffix to check for.
	 * @return bool true iff $string ends in $subString.
	 */
	static public function doesStringEndWith($string, $subString) {

		return ($subString !== '') ? substr($string, -strlen($subString)) === $subString : true;
	}

	/**
	 * @param string $string The string to examine.
	 * @param string $subString The prefix to check for.
	 * @return bool true iff $string starts with $subString.
	 */
	static public function doesStringStartWith($string, $subString) {

		return ($subString !== '') ? substr($string, 0, strlen($subString)) === $subString : true;
	}

	/**
	* This function removes the specified prefix from the string.
	*
	* @param string $string The string from which to remove the prefix.
	* @param string $prefix The prefix to remove.
	* @return string The string with the prefix removed.
	* @throws \InvalidArgumentException if $string does not begin with $prefix.
	*/
	static public function removePrefix($string, $prefix) {

		if (!Strings::doesStringStartWith($string, $prefix)) {
			throw new \InvalidArgumentException(
					$string . ' does not begin with ' . $prefix . ' in removePrefix().'
					);
		}
		return (string)substr($string, strlen($prefix));
	}

	/**
	* This function removes the specified prefix from the string, if the string begins with
	* that prefix.
	*
	* @param string $string The string from which to remove the prefix.
	* @param string $prefix The prefix to remove.
	* @return string The string with the prefix (if it was present) removed.
	*/
	static public function removePrefixIfPresent($string, $prefix) {

		if (Strings::doesStringStartWith($string, $prefix)) {
			return (string)substr($string, strlen($prefix));
		} else {
			return $string;
		}

	}

	/**
	 * @param string[] $strings
	 * @return string
	 */
	static public function joinWithAnd($strings) {
		$last = array_pop($strings);
		if ($last === null) {
			return '';
		}
		if (end($strings) === false) {
			return $last;
		}
		if (prev($strings) === false) {
			return end($strings) . ' and ' . $last;
		}
		return implode(', ', $strings) . ', and ' . $last;
	}

	/**
	 * @param string[] $strings
	 * @return string
	 */
	static public function joinWithOr($strings) {
		$last = array_pop($strings);
		if ($last === null) {
			return '';
		}
		if (end($strings) === false) {
			return $last;
		}
		if (prev($strings) === false) {
			return end($strings) . ' or ' . $last;
		}
		return implode(', ', $strings) . ', or ' . $last;
	}

	/**
	 * This is like substr(), except that in the event of an error, the return value is an empty string.  In other
	 * words, the return value is always a string.
	 *
	 * @param string $string
	 * @param int $start
	 * @param int|null $length
	 * @return string
	 */
	public static function safeSubstr($string, $start, $length = null) {
		$substr = ($length !== null)
				? substr($string, $start, $length)
				: substr($string, $start);
		return is_string($substr) ? $substr : '';
	}

	/**
	 * Truncates a UTF-8 string to a specified maximum length, appending an ellipsis if any characters are removed.
	 *
	 * @param string $string A UTF-8 string to truncate.
	 * @param int $maxLength The maximum number of characters in the returned string (including the ellipsis).
	 * @param string $ellipsis The string to append to indicate that characters are missing.
	 * @return string
	 */
	static public function truncateWithEllipsis($string, $maxLength, $ellipsis = '...') {

		if (mb_strlen($string, "UTF-8") <= $maxLength) {
			return $string;
		}

		$maxKeptLength = $maxLength - mb_strlen($ellipsis, "UTF-8");
		if ($maxKeptLength < 0) {
			return mb_substr($ellipsis, 0, $maxLength, "UTF-8");
		} else {
			return mb_substr($string, 0, $maxKeptLength, "UTF-8") . $ellipsis;
		}
	}

    /**
     * @param string $pattern A pattern as can be passed to, say, preg_match.
     * @return string|null A string describing the error, or null if the pattern is OK.
     */
	static public function getRegexError($pattern) {
        $phpErrorThrower = new PhpErrorThrower();
        try {
            if (preg_match($pattern, '') === false) {
                return "Unknown error.";
            }
        } catch (\ErrorException $e) {
            $message = str_replace("Compilation failed:", "", $e->getMessage());
            $message = str_replace("preg_match():", "", $message);
            return trim($message);
        }
        return null;
    }

}